from dataclasses import dataclass

@dataclass
class Coords:
    toolbar_width: int
    cell_size: int

    def pix_to_grid(self, coords):
        x, y = coords
        return int((x - self.toolbar_width) / self.cell_size), int(y / self.cell_size)

    def grid_to_pix(self, coords):
        x, y = coords
        return self.toolbar_width + x * self.cell_size, y * self.cell_size, self.cell_size, self.cell_size
