import pygame as pg
import sys
import pygame_gui
from pygame.locals import *
from random import randint

import tkinter
import tkinter.filedialog


from life.coords import Coords

DEFAULT_CELL_SIZE = 10
FPS = 60
SPEED_FAST = 60
SPEED_SLOW = 250

COLOR_DEAD = pg.Color('black')
COLOR_ALIVE = pg.Color('green')
COLOR_LINE = pg.Color('darkslategrey')

class App:

    @staticmethod
    def quit():
        pg.quit()
        sys.exit()

    def __init__(self, **kwargs):
        pg.init()

        display_info = pg.display.Info()
        window_width, window_height = display_info.current_w, display_info.current_h
        self.toolbar_width = int(window_height / 8)

        panel_width = window_width - self.toolbar_width
        panel_height = window_height

        self.window_shape = window_width, window_height
        self.panel_shape = panel_width, panel_height
        self.panel_rect = pg.Rect((self.toolbar_width, 0), self.panel_shape)

        self.cell_size = kwargs.get('cell_size', DEFAULT_CELL_SIZE)
        self.coords = Coords(self.toolbar_width, self.cell_size)
        self.cell_shape = int(panel_width / self.cell_size), int(panel_height / self.cell_size)

        self.GameSpeed = SPEED_FAST
        self.clock = pg.time.Clock()

        self.display = pg.display.set_mode((0,0), pg.FULLSCREEN)
        self.display.fill(pg.Color('white'))
        self.caption = pg.display.set_caption('Game of Life')
        self.in_play = False
        self.grid = self.make_empty_grid()

        self.manager = pygame_gui.UIManager(self.window_shape)
        self.init_gui()

        self.manager.draw_ui(self.display)

        # KEEP THIS AT THE END
        self.run()

    def make_empty_grid(self):
        w, h = self.cell_shape
        return [[ 0 for y in range(h)] for x in range(w)]

    def init_gui(self):
        self.manager = pygame_gui.UIManager(self.window_shape)
        self.btn = {}

        def add_button(text: str, callback = None):
            cnt = len(self.btn) + 1
            margin = 10
            width = self.toolbar_width - margin * 2
            top = width * (cnt - 1) + margin * cnt
            btn = pygame_gui.elements.UIButton(
                relative_rect=pg.Rect((margin, top), (width, width)),
                text=text,
                manager=self.manager)
            if callback is None:
                callback = lambda: print(text + " is pressed!")
            btn.on_click = callback
            return btn

        def on_start_pressed():
            btn = self.btn['RUN']
            if btn.text == 'RUN':
                btn.set_text('STOP')
                btn.select()
                self.in_play = True
            else:
                btn.set_text('RUN')
                btn.unselect()
                self.in_play = False

        def on_clear_pressed():
            self.grid = self.make_empty_grid()
            if not self.in_play:
                self.draw_cells_and_update_grid()

        def on_rand_pressed():
            self.random_fill_in()
            if not self.in_play:
                self.draw_cells_and_update_grid()

        BTN_SLOW = 'SLOW'
        BTN_FAST = 'RUSH'
        def switch_speed():
            btn = self.btn['SPEED']
            if btn.text == BTN_FAST:
                btn.set_text(BTN_SLOW)
                self.GameSpeed = SPEED_FAST
            else:
                btn.set_text(BTN_FAST)
                self.GameSpeed = SPEED_SLOW

        def file_save():
            f = tkinter.filedialog.asksaveasfile(mode='w', title="Save Game of Life state")
            if f is None:
                return
            # TODO: define how to save the grid
            # f.write()
            # f.close()

        def load_file():
            top = tkinter.Tk()
            top.withdraw()  # hide window
            path = tkinter.filedialog.askopenfilename(parent=top, title="Load Game of Life state")
            top.destroy()
            print("LOAD: ",path)

        self.btn['RUN'] = add_button('RUN', on_start_pressed)
        self.btn['SPEED'] = add_button(BTN_SLOW, switch_speed)
        self.btn['CLEAR'] = add_button('CLEAR', on_clear_pressed)
        self.btn['RAND'] = add_button('RAND', on_rand_pressed)
        self.btn['SAVE'] = add_button('SAVE', file_save)
        self.btn['LOAD'] = add_button('LOAD', load_file)
        self.btn['QUIT'] = add_button('QUIT', App.quit)


        self.manager.draw_ui(self.display)


    def draw_cell(self, x, y, alive = 1, lines = False):
        color = COLOR_ALIVE if alive else COLOR_DEAD
        coords = self.coords.grid_to_pix((x, y))
        pg.draw.rect(self.display, color, coords)

        if lines:
            w, h = self.window_shape
            px, py, _, _ = coords
            pg.draw.line(self.display, COLOR_LINE, (px, 0), (px, h))
            pg.draw.line(self.display, COLOR_LINE, (self.toolbar_width, py), (w, py))

    def draw_cells_and_update_grid(self):
        next_state = self.make_empty_grid()
        self.display.fill(COLOR_DEAD, self.panel_rect)

        w, h = self.cell_shape
        for x in range(w):
            for y in range(h):
                if self.grid[x][y] == 1:
                    self.draw_cell(x, y)
                next_state[x][y] = self.check_cells(x, y)
        self.grid = next_state

        # draw lines above cells
        w, h = self.window_shape
        for x in range(self.toolbar_width, w, self.cell_size):
            pg.draw.line(self.display, COLOR_LINE, (x, 0), (x, h))
        for y in range(0, h, self.cell_size):
            pg.draw.line(self.display, COLOR_LINE, (self.toolbar_width, y), (w, y))

    def run(self):
        delta = 0
        self.draw_cells_and_update_grid()
        is_drawing = False
        draw_alive = 1
        while True:
            for event in pg.event.get():
                if event.type == QUIT:
                    pg.quit()
                    sys.exit()

                if event.type == pygame_gui.UI_BUTTON_PRESSED:
                    event.ui_element.on_click()

                # DRAWING
                if not self.in_play:

                    if event.type == MOUSEBUTTONDOWN:
                        if event.button in (1, 3):
                            is_drawing = True
                            draw_alive = 1 if event.button == 1 else 0

                    if (is_drawing and event.type == MOUSEMOTION) or event.type == MOUSEBUTTONDOWN:
                        x, y = self.coords.pix_to_grid(event.pos)
                        if x >= 0 and not self.in_play:
                            self.grid[x][y] = draw_alive
                            self.draw_cell(x, y, draw_alive, True)

                    if event.type == MOUSEBUTTONUP:
                        is_drawing = False


                self.manager.process_events(event)
            self.manager.draw_ui(self.display)

            if self.in_play and delta >= self.GameSpeed:
                delta = 0
                self.draw_cells_and_update_grid()

            pg.display.update()
            self.manager.update(FPS)
            delta += self.clock.tick(FPS)

    def random_fill_in(self):
        w, h = self.cell_shape
        p1 = 10
        self.grid = [[1 if randint(0, 100) < p1 else 0 for y in range(h)] for x in range(w)]

    def check_cells(self, px, py):
        w, h = self.cell_shape
        count = 0
        for x in range(px-1, px+2):
            x %= w
            for y in range(py-1, py+2):
                y %= h
                if self.grid[x][y]:
                    count += 1

        if self.grid[px][py]:
            count -= 1
            if count == 2 or count == 3:
                return 1
            return 0
        else:
            if count == 3:
                return 1
            return 0

