#! usr/bin/env python3
"""
Application entry point
"""
from .app import App


def main() -> int:
    App()
    return 0

if __name__ == "__main__":
    from sys import exit
    exit(main())
