"""
Smoke tests
Purpose: verify the correct component initialization
"""

from common import life

def test_smoke():
    assert life is not None

if __name__ == "__main__":
    life.main()
