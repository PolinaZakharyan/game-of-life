Conway's Game of Life
---



# Installation

Run `pip install -e .` to install the package in developers mode.
This will also create scripts defined in the `project.scripts` section
in [pyproject.toml](pyproject.toml).
With the `-e` option you don't need to reinstall the package on each update
if the entry points are compatible.

# Verification

Install test dependencies: `pip install pytest pytest-cov tox`.

To execute the tests, run the `tox` command in the project directory.
The test configuration is defined in the [tox.ini](tox.ini) file.

To check the coverage reports, run `browse ./htmlcov/index.html`
